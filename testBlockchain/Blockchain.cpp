#include "Blockchain.h"

Blockchain::Blockchain() {
	Chain.emplace_back(Block(0, "Genesis block"));
	Difficulty = 4;
}

void Blockchain::AddBlock( Block NewBlock)
{	
	NewBlock.previousHash = GetLastBlock().GetHash();
	NewBlock.MineBlock(Difficulty);
	Chain.push_back(NewBlock);
}

Block Blockchain::GetLastBlock() const
{
	return Chain.back();
}