#pragma once
#include <iostream>
#include <cstdint>
#include <ctime>


class Block {
	/* ---------------------------------------------- */
	/* Desc : Les diff�rentes variables et methodes contenus dans un block
		   Index : L'index du block dans la chaine
		   Nonce : ( A redefinir )
		   Time : Horodatage du Nonce, assurant la pr�sence d'un unique Nonce
		   Data : Les donn�e contenues par le bloc, elle peuvent etre de nimporte quel type
		   Hash : Le hash associ� au block

		   GetHash() : methode permettant der�cup�rer le Hash
		   CalculateHash() : Methode permettant de calculer le Hash, pour cr�er la PoW ( Proof of Work )
		   MineBlock() : Methode pemettant de miner un block
	*/
public:
	//Contient le hash du pr�c�dent block de la chain
	std::string previousHash;

	Block(uint32_t IndexIn, const std::string &DataIn);

	std::string GetHash();

	void MineBlock(uint32_t	Difficulty);

private:

	uint32_t Index;
	int64_t Nonce;
	std::time_t Time;
	std::string Data;
	std::string Hash;


	std::string CalculateHash() const;

};