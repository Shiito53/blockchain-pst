#pragma once
#include <cstdint>
#include <vector>
#include "block.h"

class Blockchain {

	/* ---------------------------------------------- */
	/* Desc : Les diff�rentes variables et methodes contenus dans un Blockchain
			Difficulty : D�finie le nombre de 0 au d�but du hash a calculer afin de rendre hash plus difficle a calculer
			Chain : Tableau de Block representant la chaine de Block
			
			GetLastBlock() : Renvoie le block avant le block courant
			AddBlock() : Permet de rajouter un block a la chaine
	*/
public:
	Blockchain();

	void AddBlock(Block NewBlock);

private:	
	uint32_t Difficulty;
	std::vector<Block> Chain;

	Block GetLastBlock() const;


};