#include "block.h"
#include "sha256.h"
#include <vector>
#include <sstream>


Block::Block(uint32_t IndexIn, const std::string& DataIn) : Index(IndexIn), Data(DataIn) {

	Nonce = -1;
	Time = std::time(nullptr);
}


std::string Block::GetHash()
{
	return Hash;
}

void Block::MineBlock(uint32_t difficulty)
{
	char* cstr = (char*)malloc((difficulty + 1) * sizeof(char));
	for (uint32_t i = 0; i < difficulty; ++i) {
		cstr[i] = '0';
	}
	cstr[difficulty] = '\0';

	std::string str(cstr);

	do {
		Nonce++;
		Hash = CalculateHash();
	} while (Hash.substr(0, difficulty) != str);
	std::cout << "Block Mined: " << Hash << std::endl;
}

inline std::string Block::CalculateHash() const
{
	std::stringstream ss;
	ss << Index << Time << Data << Nonce << previousHash;

	return sha256(ss.str());
}